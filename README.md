# User System

---

## Sistema de cadastro e edição de usuários


![TypeScript](https://img.shields.io/badge/-TypeScript-007ACC?style=flat&logoColor=fff&logo=typescript)
![React](https://img.shields.io/badge/-React-0488B0?style=flat&logoColor=fff&logo=react)



##  Donwload do Projeto

Para o download do projeto você precisa estar com as configurações requisitadas do React.Js já instaladas.

Feito isso, Faça:

 1 - O clone o projeto.

```bash

git clone (endereço necessário para clonar)

```

2 - Entre na pasta do projeto.

3 - Instale as dependências,  abra o promt de comando na raiz do projeto e digite:

(Neste projeto utilizei o __yarn__, mas pode ser usado o __npm__ )

* **yarn**
```bash
yarn
```
4 - Para iniciar a aplicação, abra o promt de comando na raiz do projeto e:

 4.1 - Inicie a fake API digitando:
 
* **yarn**
```bash
yarn server
```

 4.2 - Inicie a aplicação  digitando:
 
* **yarn**
```bash
yarn start
```
